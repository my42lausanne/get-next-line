/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 12:03:45 by dfarhi            #+#    #+#             */
/*   Updated: 2021/11/01 16:04:48 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	*ft_calloc(size_t count, size_t size)
{
	void	*ptr;

	ptr = malloc(count * size);
	if (!ptr)
		return (0);
	ft_bzero(ptr, count * size);
	return (ptr);
}

void	ft_bzero(void *s, size_t n)
{
	int		i;
	char	*str;

	str = s;
	i = -1;
	while ((unsigned int)++i < n)
		str[i] = 0;
}

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned long	i;
	char			*d;
	const char		*s;

	if (!dest && !src)
		return (0);
	d = dest;
	s = src;
	i = -1;
	while (++i < n)
		d[i] = s[i];
	return (dest);
}

size_t	ft_strlen(const char *s)
{
	int	c;

	c = 0;
	while (s[c] != 0)
		c++;
	return (c);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*ptr;
	size_t	len1;
	size_t	len2;

	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	ptr = ft_calloc(len1 + len2 + 1, sizeof(char));
	if (!ptr)
		return (0);
	ft_memcpy(ptr, s1, len1);
	ft_memcpy(&ptr[len1], s2, len2);
	ptr[len1 + len2] = 0;
	return (ptr);
}
