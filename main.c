/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/25 12:03:38 by dfarhi            #+#    #+#             */
/*   Updated: 2021/11/01 18:05:57 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

int	main(int ac, char **av)
{
	int		fd;
	char	*line;
	int		c;

	if (ac < 2)
		fd = 1;
	else
		fd = open(av[1], O_RDONLY);
	printf("BUFFER_SIZE = %lu\nReading from fd %d\n", (size_t)BUFFER_SIZE, fd);
	line = ft_calloc(1, sizeof(char));
	c = 0;
	while (!c || line)
	{
		free(line);
		line = get_next_line(fd);
		printf("line %d - '%s'\n", c++, line);
	}
}
